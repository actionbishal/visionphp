<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type,application/x-www-form-urlencoded, X-Auth-Token, Origin, Authorization');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('document', 'DocumentController');
    Route::post('detect', 'DetectionController@detect_text');
    Route::get('details', 'AuthController@details');
    Route::resource('line-item','LineItemsController');
    Route::resource('basic-info','BasicInfoController');
    Route::post('document/table','DocumentController@table');

//    Route::post('process/{id}', 'ProcessController@process');

});
Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
//Route::post('login', [ 'as' => 'login']);




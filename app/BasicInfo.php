<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasicInfo extends Model
{
    protected $fillable=[
        'invoice_no','date','organization_name','address','total','sub_total','document_id'
    ];
    public function document(){
        return $this->belongsTo(Document::class);
    }
}

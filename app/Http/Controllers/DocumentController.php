<?php

namespace App\Http\Controllers;

use App\BasicInfo;
use App\Company;
use App\Document;
use App\Events\Illuminate\Auth\Events\ProcessImage;
use App\Jobs\ImageProcessJob;
use App\LineItem;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;

class DocumentController extends Controller
{
    public function __constructor()
    {
        $this->middleware('api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        return Document::select()->orderBy('created_at','Desc')->get();
    }
    public function table(Request $request){
      return $query = Document::select()
                ->when($request->status == "Processed",
            function($q) use ($request) {
                return $q->where('status',$request->status);
            }
        )
          ->when($request->status=="Reviewing",
              function($q) use ($request) {
              return $q->where('status',$request->status);
              }
              )
          ->when($request->status=="Reviewed",
              function($q) use ($request) {
                  return $q->where('status',$request->status);
              }
          )->get();

//        dd($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Auth\Authenticatable
     */
    public function store(Request $request)
    {
        foreach ($request->file() as $file)
            $originalName = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $newName = time() . '.' . $ext;
        $file->storeAs('public/uploads/documents/' . Auth::user()->company->id, $newName);

        $api_token = Token::select()->first()->token;

        $http = new \GuzzleHttp\Client();
        $response = $http->post(env('TAB_SCANNER_URL') . $api_token . '/process', [
            'multipart' => [
                [
                    'name' => 'receiptImage',
                    'contents' => fopen(storage_path('app\\public\\uploads\\documents\\' . Auth::user()->company->id . '\\' . $newName), 'rb'),
                    'filename' => $newName,
                ],
            ],
        ]);
//        dd(json_decode($response->getBody()));

        $tokenMsg = json_decode($response->getBody());
//        dd($token);

//        if (!array_key_exists('token', $tokenMsg)) {
        if($tokenMsg->message=="ERROR_CLIENT: Not enough credit")
        {
            $processToken = Token::where('token', $api_token)->first();
            $processToken->delete();
            $api_token = Token::select()->first()->token;
            $http = new \GuzzleHttp\Client();
            $response = $http->post(env('TAB_SCANNER_URL') . $api_token . '/process', [
                'multipart' => [
                    [
                        'name' => 'receiptImage',
                        'contents' => fopen(storage_path('app\\public\\uploads\\documents\\' . Auth::user()->company->id . '\\' . $newName), 'rb'),
                        'filename' => $newName,
                    ],
                ],
            ]);
        }
        $token = json_decode($response->getBody())->token;
        $company_id = Auth::user()->company->id;
//        if ($token)

        $document = Document::create([
            'name' => $originalName,
            'filename' => $newName,
            'company_id' => $company_id,
            'token' => $token
        ]);

        $channel = 'document.' . Auth::user()->company->id;
        $job = (new ImageProcessJob($token, $api_token, $channel, $document->id))->delay(Carbon::now()->addSeconds(4));
        dispatch($job);
        return response()->json(Document::find($document->id));

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doc = Document::where('id', $id)->update([
            'status' => "Reviewing"
        ]);

        return response()->json(['document' => Document::where('id', $id)->with(['basicInfo', 'lineItems'])->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Document::where('id', $id)->update([
            'status' => "Reviewed"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Document::where('id', $id)->delete();
    }
}

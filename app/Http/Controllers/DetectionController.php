<?php

namespace App\Http\Controllers;

use App\Document;
use Google\ApiCore\ApiException;
use Google\ApiCore\ValidationException;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetectionController extends Controller
{

    /**
     * @param Request $request
     * @return array|\Exception|ValidationException
     * @throws ApiException
     */
    public function detect_text(Request $request)
    {

        $image_name=Document::select('filename')->where('id',$request->data)->get();
//        dd($image_name[0]->image);
//dd('app\\public\\uploads\\documents\\' . Auth::user()->company()->first()->id . '\\' . $image_name[0]->filename);
        $path = storage_path('app\\public\\uploads\\documents\\' . Auth::user()->company()->first()->id . '\\' . $image_name[0]->filename);
//        $path = 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/ReceiptSwiss.jpg/800px-ReceiptSwiss.jpg';


        putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\xampp\htdocs\visionphp\apikey.json');
        # annotate the image
        $image = file_get_contents($path);
        try {
            $imageAnnotator = new ImageAnnotatorClient();
            $response = $imageAnnotator->documentTextDetection($image);
            $texts = $response->getTextAnnotations();
//            printf('%d texts found:' . PHP_EOL, count($texts));
            $original_texts = $modified_texts = [];

            $new_text_arr[0] = $texts[0]->getBoundingPoly()->getVertices()[0]->getX() . $texts[0]->getBoundingPoly()->getVertices()[0]->getY();
            foreach ($texts as $index => $text) {
                if ($index > 0) {
//                    echo ($text->getDescription() . '<br>');
                    $data = $text->getDescription();
                    # get bounds
                    $vertices = $text->getBoundingPoly()->getVertices();
                    $bounds = [];
                    foreach ($vertices as $vertex) {
//                        $bounds[] = sprintf('(%d,%d)', $vertex->getX(), $vertex->getY());

                        array_push($bounds, [$vertex->getX(), $vertex->getY()]);
                    }
//                    $msg=['text'=>$data,'bounds' =>$bounds];

//                    echo('Bounds: ' . join(', ', $bounds) . '<br>');
                    $original_texts[] = ['text' => $data, 'bounds' => $bounds];
//                    dd($original_datas);

                    $i = $index;
                    while (
                        $i < sizeof($texts) - 1 &&
                        ($texts[$i + 1]->getDescription() != ':' && $texts[$i]->getDescription() != ':') &&
                        abs($texts[$i + 1]->getBoundingPoly()->getVertices()[0]->getY() - $texts[$i]->getBoundingPoly()->getVertices()[1]->getY()) < 5
                        &&
                        $texts[$i + 1]->getBoundingPoly()->getVertices()[0]->getX() - $texts[$i]->getBoundingPoly()->getVertices()[1]->getX() < 20
                    ) {

//                        dd($data . $texts[$i + 1]->getDescription());
                        $new_text_arr [$i + 1] = $texts[$i + 1]->getBoundingPoly()->getVertices()[0]->getX() . $texts[$i + 1]->getBoundingPoly()->getVertices()[0]->getY();
                        $data .= ' ' . $texts[$i + 1]->getDescription();
                        $bounds[1] = [$texts[$i + 1]->getBoundingPoly()->getVertices()[1]->getX(), $texts[$i + 1]->getBoundingPoly()->getVertices()[1]->getY()];
                        $bounds[2] = [$texts[$i + 1]->getBoundingPoly()->getVertices()[2]->getX(), $texts[$i + 1]->getBoundingPoly()->getVertices()[2]->getY()];
                        $i++;
//                        dd($data);

                    }
//                    if (!strpos($new_text_str, $text->getBoundingPoly()->getVertices()[0]->getX() . $text->getBoundingPoly()->getVertices()[0]->getY()))
                    if (!in_array($text->getBoundingPoly()->getVertices()[0]->getX() . $text->getBoundingPoly()->getVertices()[0]->getY(), $new_text_arr))
                        $modified_texts[] = ['text' => $data, 'bounds' => $bounds];
                }

            }
//            dd($modified_datas);

            $imageAnnotator->close();
//            $class = new compareImages;
//            echo $class->compare('1.jpg','2.jpg');

            return ['original_texts' => $original_texts, 'modified_texts' => $modified_texts, 'dimension' => getimagesize($path),'filename'=>$image_name[0]->image];

        } catch (ValidationException $e) {
            return $e;
        }


    }
}

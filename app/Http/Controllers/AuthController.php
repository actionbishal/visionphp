<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        Validator::make($request->all(), [
            'email' => 'required|String|email',
            'password' => 'required'

        ]);

        $user = User::where('email', $request->email)->first();
        if ($user) {
            $http = new Client();
            $response = $http->post(env('APP_URL') . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('PASSPORT_CLIENT_ID'),
                    'client_secret' => env('PASSPORT_CLIENT_SECRET'),
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '',
                ],
            ]);
//                $company_details=json_decode($user->company()->get()[0],true);
            return response(json_decode((string)$response->getBody(), true));
        }

//
//         {
//                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
//
//                $response = ['token' => $token];
//                dd(Auth::user()->email);
//
//                return response($response, 200);
//            } else {
//                $response = "Password missmatch";
//                return response($response, 422);
//            }
//
//        } else {
//            $response = 'User does not exist';
//            return response($response, 422);
//        }
    }

    public function register(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|String|max:255',
            'companyName' => 'required|String|max:255',
            'email' => 'required|String|email|unique:users',
            'password' => 'required'

        ]);

//        if ($validator->fails())
//        {
//            return response(['errors'=>$validator->errors()->all()], 422);
//        }
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if ($user) {
            $company = Company::create(['name' => $request->companyName,'user_id'=>$user->id]);

            $http = new \GuzzleHttp\Client();
            $response = $http->post(env('APP_URL') . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('PASSPORT_CLIENT_ID'),
                    'client_secret' => env('PASSPORT_CLIENT_SECRET'),
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '',
                ],
            ]);
            return response(json_decode((string)$response->getBody(), true));
        }
    }

    public function details()
    {
        return response()->json(['user' => auth()->user(), 'company' => auth()->user()->company()->first()], 200);
    }
}

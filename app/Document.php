<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Document extends Model
{
    protected $fillable = [
        'name', 'filename', 'company_id', 'token'
    ];
    protected $appends = ['image','image_size'];

    protected $attributes = [
        'type' => "Receipt"
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function getImageAttribute()
    {
        return env('APP_URL') . '/storage/uploads/documents/' . $this->company_id . '/' . $this->filename;
    }

    public function getImageSizeAttribute()
    {
       $image= getimagesize(storage_path('app\\public\\uploads\\documents\\' . Auth::user()->company->id . '\\' . $this->filename));
       return $image;
    }

    public function basicInfo()
    {
        return $this->hasOne(BasicInfo::class);
    }

    public function lineItems()
    {
        return $this->hasMany(LineItem::class);
    }
}

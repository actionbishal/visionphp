<?php

namespace App\Jobs;

use App\BasicInfo;
use App\Document;
use App\Events\ProcessImageEvent;
use App\LineItem;
use App\OtherInfo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImageProcessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $token, $document_id,$api_token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($token,$api_token, $channel, $document_id)
    {
        $this->token = $token;
        $this->api_token = $api_token;
        $this->document_id = $document_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $http = new \GuzzleHttp\Client();
        $response = $http->get(env('TAB_SCANNER_URL') . $this->api_token . '/result/' . $this->token);
        $result = json_decode($response->getBody());

        BasicInfo::create([
            'invoice_no' => '146972',
            'date' => $result->result->date,
            'organization_name' => $result->result->establishment,
            'address' => $result->result->address,
            'total' => $result->result->total,
            'sub_total' => $result->result->subTotal,
            'document_id' => $this->document_id
        ]);
        foreach ($result->result->lineItems as $lineItem) {
            LineItem::create([
                'name' => $lineItem->desc,
                'product_code' => $lineItem->productCode,
                'quantity' => $lineItem->qty,
                'price' => $lineItem->price,
                'total' => $lineItem->lineTotal,
                'document_id' => $this->document_id
            ]);
        }
        $info[] = json_encode($result->result->summaryItems);
        OtherInfo::create([
            'information' => "hello",
            'document_id' => $this->document_id
        ]);
        $document = Document::where('token', 'Like', $this->token);
        $document->update(['status' => 'Processed']);
//        dd($document->first());
        event(new ProcessImageEvent($document->first()->id));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    protected $fillable=[
        'name','product_code','quantity','price','total','document_id'
    ];
    public function document(){
        return $this->belongsTo(Document::class);
    }
}

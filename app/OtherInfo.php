<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherInfo extends Model
{
    protected $casts=[
        'information'=>'array'
    ];
    protected $fillable=[
        'document_id'
    ];

}

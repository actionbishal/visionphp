import axios from "axios";
// import VueRouter from "vue-router";
// axios.defaults.headers.common['Authorization'] = 'Bearer '+ this.$store.getters.token;

const baseUrl = process.env.MIX_APP_URL
const state = {
    receipts: [],
    loaded: true,
    token: localStorage.getItem('token') || null,
    // status: '',
    user: {},
    company: {},
    company_id:''
}

const mutations = {
    setToken(state, payload) {
        state.token = payload.token;
    },
    setSignOut(state) {
        state.token = ''
        state.user = ''
        state.company = ''
    },
    setUserDetails(state, payload) {
        state.user = payload.data.user
        state.company = payload.data.company
        state.company_id=payload.data.company.id
    }
}

const actions = {
    async signUp({commit}, payload) {
        let response = await axios.post(`register`, payload)
        const token = 'Bearer ' + response.data.access_token
        localStorage.setItem('token', token)
        commit('setToken', token)
    },

    async login({commit}, payload) {
        let response = await axios.post(`login`, payload)
        const token = 'Bearer ' + response.data.access_token
        localStorage.setItem('token', token)
        commit('setToken', token)
    },

    async signOut({commit}) {
        let response = await commit('setSignOut')
        localStorage.removeItem('token')
        delete axios.defaults.headers.common['Authorization']
        commit('setSignOut')

    },
    async getUserDetails({commit}) {
        let response = await axios.get(`details`)
        commit('setUserDetails', response)
    }
}
const getters = {
    isLoggedIn: state => !!state.token,
    userDetails: state => state.user,
    companyDetails: state => state.company,
    companyId:state=>state.company_id
}

export default {
    state,
    mutations,
    actions,
    getters
}

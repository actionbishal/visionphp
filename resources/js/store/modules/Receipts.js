import Authentication from './Authentication'
import axios from 'axios'

const state = {
    receipts: [],
    loaded: false,
    receiptImage: '',

}

const mutations = {
    setReceiptImage(state, payload) {
        state.receiptImage = payload
        state.loaded = true
    },

    set_receipts(state, payload) {
        console.log(payload)
        state.receipts = payload
    },
    add_new_receipt(state, payload) {
        console.log(payload)
        state.receipts.push(payload)
    },
    updateReceipt(state, payload) {
        state.receipts.find(x => x.id === payload.id).status = payload.status

    },
    closeImage(state) {
        state.loaded = false
    },
    removeReceipt(state, payload) {

        let index = state.receipts.findIndex(x => x.id === payload.id)
        state.receipts.splice(index, 1)
    }

}

const actions = {
    async getReceipts({commit},payload) {
        let response = await axios.post(`document/table`,payload)
        console.log(response)

        commit('set_receipts', response.data);
    },
    async getDocument({commit}, data) {
        let response = await axios.get('document/' + data.document_id)
        return response.data
    },
    async addLineItem({commit}, data) {
        return await axios.get('line-item/' + data.document_id + '/edit')
    },
    async deleteLineItem({commit}, data) {
        await axios.delete('line-item/' + data.lineItem_id)
    },
    async deleteReceipt({commit}, data) {
        console.log(data)
        await axios.delete('document/' + data)
        let index = state.receipts.findIndex(x => x.id === data)
        state.receipts.splice(index, 1)
    }

}
const getters = {
    Receipts: state => state.receipts,
    receiptImage: state => state.receiptImage,
    Loaded: state => state.loaded,
}


export default {
    state,
    mutations,
    getters,
    actions
}

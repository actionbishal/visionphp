import axios from "axios";

const baseUrl = process.env.MIX_APP_URL

const state = {
    field_box: {
        display: false,
        text: '',
        top: '',
        left: '',
        width: '',
    },
    texts: {},
    modified_texts: {},
    imageWidth: 0,
    imageHeight: 0,
    filename: '',
    selectedField:'sidebar-field-input-1001001'
}

const mutations = {

    setText(state, payload) {

        state.texts = payload.data.original_texts
        state.modified_texts = payload.data.modified_texts
        state.filename = payload.data.filename
    },
    setSelectedField(state,payload){
        state.selectedField=payload.toString()

    },
    setSelected(state,payload){
        state.field_box.text=payload
    },
    // selectedReceipt(state,payload){
    //     state.filename=payload.image
    //     console.log(state.filename)
    // }
    updateFieldBox(state,payload) {
        state.field_box = payload
    },
    setFieldBoxText(state,payload) {
        state.field_box.text = payload
    }
}

const actions = {
    // getText({commit}) {
    //     axios.get(`${baseUrl}/api/detect`)
    //         .then(response => {
    //             commit('setText', response)
    //         })
    //
    // },
    async getText({commit}, payload) {
        console.log(payload)
        let data = {data: payload}
        let response = await axios.post(`detect`, data)
        commit('setText', response)
    },
}
const getters = {
    Texts: state => state.texts,
    ModifiedTexts: state => state.modified_texts,
    imageWidth: state => state.imageWidth,
    imageHeight: state => state.imageHeight,
    imageFilename: state => state.filename,
    selectedField:state=>state.selectedField,
    field_box: state=> state.field_box
}
export default {
    state,
    mutations,
    getters,
    actions
}

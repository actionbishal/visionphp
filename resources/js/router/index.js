import Vue from "vue"
import Router from "vue-router"
import Home from "../views/Home.vue"
import Landing from "../views/Landing.vue"
import Dashboard from "../Dashboard.vue"
// import ReviewImage from "../components/ReviewImage"
import Documents from "../views/Documents.vue"
import ProcessedDocuments from "../views/ProcessedDocuments.vue"
import ReviewedDocuments from "../views/ReviewedDocuments.vue"
import ReviewingDocuments from "../views/ReviewingDocuments.vue"

import store from "../store"

import ReviewDocument from "../ReviewDocument.vue"

Vue.use(Router)


const router = new Router({

    // linkActiveClass: 'active',
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'Landing',
            component: Landing,
            meta: {
                title: 'Landing',
                isVisitor: true
            }
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                title: 'Dashboard',
                requiresAuth: true
            },
            children: [
                {
                    path: '/',
                    name: 'Home',
                    component: Home,
                    meta: {
                        title: 'Home'
                    }
                },
                // {
                //     path: 'image',
                //     name: 'ReviewImage',
                //     component: ReviewImage,
                //     meta: {
                //         title: 'ReviewImage'
                //     }
                // },
                {
                    path: 'document',
                  name: 'Documents',
                    component: Documents,
                },
                {
                    path:'processed',
                    name:'ProcessedDocuments',
                    component:ProcessedDocuments
                },
                {
                    path:'reviewing',
                    name:'ReviewingDocuments',
                    component:ReviewingDocuments
                },
                {
                    path:'reviewed',
                    name:'ReviewedDocuments',
                    component:ReviewedDocuments
                }
            ],
        },
        {
            path: '/dashboard/document/:id/review',
            name:'ReviewDocument',
            component:ReviewDocument,
            beforeRouteLeave (to, from , next) {
                // const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
                // if (answer) {
                //     next()
                // } else {
                //     next(false)
                // }
                console.log("Byeeee")
            }
        },
        // {
        //     path: '*',
        //     redirect: '/'
        // },
    ],

})
router.beforeEach((to, from, next) => {

    if (to.matched.some(record => record.meta.isVisitor)) {
        if (store.getters.isLoggedIn) {
            next({path: '/dashboard'})
        return
        }
        next()
    }
    else if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters.isLoggedIn) {
            next({path:'/'})
        return
        }
            next()

    }
    else {
        next()
    }
})

// router.afterEach((to, from) => {
//     this.$Progress.finish()
// })


export default router
